package test_work.tsymbal.andrew.textsnake.game_logic.spawner;

import java.util.Random;

import test_work.tsymbal.andrew.textsnake.models.game_models.FoodModel;
import test_work.tsymbal.andrew.textsnake.models.game_models.GameModel;
import test_work.tsymbal.andrew.textsnake.models.game_models.bonus_models.MoreFoodBonusModel;
import test_work.tsymbal.andrew.textsnake.models.game_models.bonus_models.SpeedDownBonusModel;
import test_work.tsymbal.andrew.textsnake.models.game_models.bonus_models.SpeedUpBonusModel;

public class EffectSpawner extends GameObjectSpawner {

    public EffectSpawner(int fieldWeight, int fieldHeight) {
        super(fieldWeight, fieldHeight);
    }

    @Override
    protected GameModel getGameObjForSpawn(int x, int y) {

        int luckyMonsterIndex = new Random().nextInt(2);

        switch (luckyMonsterIndex) {
            case 0:
                return new SpeedUpBonusModel(x, y);
            case 1:
                return new SpeedDownBonusModel(x, y);
            case 2:
                return new MoreFoodBonusModel(x, y);
        }

        return new SpeedUpBonusModel(x, y);
    }
}
