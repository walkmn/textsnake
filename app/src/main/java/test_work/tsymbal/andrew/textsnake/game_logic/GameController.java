package test_work.tsymbal.andrew.textsnake.game_logic;

import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import test_work.tsymbal.andrew.textsnake.MainActivity;
import test_work.tsymbal.andrew.textsnake.TimerController;
import test_work.tsymbal.andrew.textsnake.models.GameFieldModel;
import test_work.tsymbal.andrew.textsnake.models.PointModel;
import test_work.tsymbal.andrew.textsnake.models.game_models.SnakeModel;

/**
 * Created by user on 26.03.2016.
 */
public class GameController implements GameFieldModel.GameFieldListener, View.OnTouchListener {

    private boolean isStarted = false;
    private GameFieldModel gameFieldModel;
    private final TimerController timerController;

    private Activity activity;
    private TextView tvGameField;

    private GameControllerListener gameControllerListener;

    public GameController(Activity activity, TextView tvGameField, TextView tvTimeCounter, GameControllerListener gameControllerListener) {
        this.activity = activity;
        this.tvGameField = tvGameField;
        this.gameControllerListener = gameControllerListener;

        this.gameFieldModel = new GameFieldModel(MainActivity.FIELD_WIDTH, MainActivity.FIELD_HEIGHT, this);
        this.timerController = new TimerController(activity, tvTimeCounter);

        this.tvGameField.setOnTouchListener(this);
    }

    public void start() {
        isStarted = true;
    }

    public void pause() {
        isStarted = false;
    }

    public void updateFrame(int time) {
        if (!isStarted) return;

        try {

            gameFieldModel.updateFrame(time);
            timerController.updateFrame(time);

        } catch (ArrayIndexOutOfBoundsException e) {
            gameOver();
        }
    }


    public void restart() {
        gameFieldModel = new GameFieldModel(MainActivity.FIELD_WIDTH, MainActivity.FIELD_HEIGHT, this);
        timerController.reset();
    }

    @Override
    public void onUpdatedField(final String result) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvGameField.setText(result);
            }
        });
    }

    @Override
    public void gameOver() {
        isStarted = false;
        gameControllerListener.onGameOver();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (!isStarted) return false;

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:

                float touchX = event.getX();
                float touchY = event.getY();

                float coeffTouchX = touchX / (float) tvGameField.getWidth();
                float coeffTouchY = touchY / (float) tvGameField.getHeight();

                PointModel stakeHead = gameFieldModel.getSnake().getHeadPoint();

                float coeffSnakeX = (float) stakeHead.getX() / (float) MainActivity.FIELD_WIDTH;
                float coeffSnakeY = (float) stakeHead.getY() / (float) MainActivity.FIELD_HEIGHT;

                SnakeModel.Direction direction = null;

                switch (gameFieldModel.getSnake().getCurrentDirection()) {
                    case DOWN:
                        direction = (coeffTouchX < coeffSnakeX) ? SnakeModel.Direction.LEFT : SnakeModel.Direction.RIGHT;
                        break;

                    case UP:
                        direction = (coeffTouchX < coeffSnakeX) ? SnakeModel.Direction.LEFT : SnakeModel.Direction.RIGHT;
                        break;

                    case LEFT:
                        direction = (coeffTouchY > coeffSnakeY) ? SnakeModel.Direction.DOWN : SnakeModel.Direction.UP;
                        break;

                    case RIGHT:
                        direction = (coeffTouchY > coeffSnakeY) ? SnakeModel.Direction.DOWN : SnakeModel.Direction.UP;
                        break;
                }

                gameFieldModel.getSnake().changeDirection(direction);

                return false;
        }
        return false;
    }

    public void setDifficultLevel(int level) {
        switch (level) {
            case 0 : gameFieldModel.getSnake().setDifficultValue(0); break;
            case 1 : gameFieldModel.getSnake().setDifficultValue(2); break;
            case 2 : gameFieldModel.getSnake().setDifficultValue(4); break;
        }

    }
}
