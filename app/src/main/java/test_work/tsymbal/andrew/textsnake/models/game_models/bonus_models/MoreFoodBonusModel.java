package test_work.tsymbal.andrew.textsnake.models.game_models.bonus_models;

import test_work.tsymbal.andrew.textsnake.models.game_models.AffectingSnake;
import test_work.tsymbal.andrew.textsnake.models.game_models.GameModel;
import test_work.tsymbal.andrew.textsnake.models.game_models.SnakeModel;

public class MoreFoodBonusModel extends GameModel implements AffectingSnake {

    public MoreFoodBonusModel(int startX, int startY) {
        super(startX, startY);
    }

    @Override
    public void updateFrame(int time) {

    }

    @Override
    public int getLifeTime() {
        return 12000;
    }

    @Override
    public char getPersonCharacter() {
        return '~';
    }

    @Override
    public void affect(SnakeModel snake) {
        killYourSelf();

        snake.increaseLength();
        snake.increaseLength();

        snake.decreaseSpeed();
    }
}
