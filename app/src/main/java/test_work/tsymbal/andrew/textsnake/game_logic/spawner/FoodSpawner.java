package test_work.tsymbal.andrew.textsnake.game_logic.spawner;

import java.util.Random;

import test_work.tsymbal.andrew.textsnake.models.game_models.FoodModel;
import test_work.tsymbal.andrew.textsnake.models.game_models.GameModel;
import test_work.tsymbal.andrew.textsnake.models.game_models.bonus_models.MoreFoodBonusModel;
import test_work.tsymbal.andrew.textsnake.models.game_models.bonus_models.SpeedDownBonusModel;
import test_work.tsymbal.andrew.textsnake.models.game_models.bonus_models.SpeedUpBonusModel;

public class FoodSpawner  extends GameObjectSpawner {

    public FoodSpawner(int fieldWeight, int fieldHeight) {
        super(fieldWeight, fieldHeight);
    }

    @Override
    protected GameModel getGameObjForSpawn(int x, int y) {
        return new FoodModel(x, y);
    }
}
