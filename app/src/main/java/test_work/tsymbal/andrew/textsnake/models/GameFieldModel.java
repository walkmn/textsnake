package test_work.tsymbal.andrew.textsnake.models;

import java.util.ArrayList;

import test_work.tsymbal.andrew.textsnake.game_logic.spawner.EffectSpawner;
import test_work.tsymbal.andrew.textsnake.game_logic.spawner.FoodSpawner;
import test_work.tsymbal.andrew.textsnake.game_logic.spawner.GameObjectSpawner;
import test_work.tsymbal.andrew.textsnake.models.game_models.AffectingSnake;
import test_work.tsymbal.andrew.textsnake.models.game_models.GameModel;
import test_work.tsymbal.andrew.textsnake.models.game_models.SnakeModel;

public class GameFieldModel implements SnakeModel.SnakeListener {

    private SnakeModel snake;

    private GameFieldListener gameFieldListener;
    private GameObjectSpawner foodSpawner;
    private GameObjectSpawner effectSpawner;

    private char [][] field;
    private char [][] convertedArr;
    private ArrayList<GameModel> gameObjects = new ArrayList<>();


    public GameFieldModel(int weight, int height, GameFieldListener gameFieldListener) {
        snake = new SnakeModel(10, 10, this);
        this.field = new char[weight][height];
        this.convertedArr = new char[height][weight];
        this.gameFieldListener = gameFieldListener;

        foodSpawner = new FoodSpawner(weight, height);
        effectSpawner = new EffectSpawner(weight, height);

        cleanField();

        gameObjects.add(snake);
        gameObjects.add(foodSpawner.spawn());

        gameFieldListener.onUpdatedField(generateStringForDrawing());
    }

    private void addGameObject(GameModel gameModel) {
        if (gameModel == null) return;

        gameObjects.add(gameModel);
    }

    public void updateFrame(int time) {
        for (GameModel gameObj : gameObjects) {
            gameObj.updateFrame(time);
        }

        addGameObject(foodSpawner.tryToSpawn(time));
        addGameObject(effectSpawner.tryToSpawn(time));
    }

    private boolean isCollisionGameObjects(GameModel firstGameObj, GameModel secondGameObj) {
        for (PointModel pointFirstObj : firstGameObj.getPoints()) {
            for (PointModel pointSecondObj : secondGameObj.getPoints()) {
                if (pointFirstObj.getX() == pointSecondObj.getX() && pointFirstObj.getY() == pointSecondObj.getY()) {
                    return true;
                }
            }
        }
        return false;
    }

    private String generateStringForDrawing() {

        for (int x = 0; x < field.length; x++) {
            for (int y = 0; y < field[x].length; y++) {
                convertedArr[y][x] = field[x][y];
            }
        }

        StringBuilder result = new StringBuilder();
        for (int x = 0; x < convertedArr.length; x++) {
            for (int y = 0; y < convertedArr[x].length; y++) {
                result.append(convertedArr[x][y]);
            }
            result.append('\n');
        }

        return result.toString();
    }

    private void fillField() {
        for (GameModel gameObj : gameObjects) {
            for (PointModel point:
                    gameObj.getPoints()){
                field[point.getX()][point.getY()] = gameObj.getPersonCharacter();
            }
        }
    }

    private void cleanField() {
        for (int x = 0; x < field.length; x++) {
            for (int y = 0; y < field[x].length; y++) {
                field[x][y] = ' ';
            }
        }
    }

    public SnakeModel getSnake() {
        return snake;
    }

    @Override
    public void makeAStep() {
        for (GameModel gameObj : gameObjects) {
            if (gameObj instanceof AffectingSnake && isCollisionGameObjects(snake, gameObj)) {
                ((AffectingSnake) gameObj).affect(snake);
            }
        }

        cleanField();
        fillField();

        gameFieldListener.onUpdatedField(generateStringForDrawing());
    }

    @Override
    public void died() {
        gameFieldListener.gameOver();
    }

    /**
     * Created by user on 23.03.2016.
     */
    public interface GameFieldListener {
        void onUpdatedField(String result);
        void gameOver();
    }
}
