package test_work.tsymbal.andrew.textsnake.models.game_models;

import java.util.Random;

import test_work.tsymbal.andrew.textsnake.models.PointModel;

public class SnakeModel extends GameModel {

    private static final float MAX_SPEED = 10;
    private static final float MIN_SPEED = 1;

    private Direction currentDirection;
    private int currentLength = 3;
    private float currentSpeed = 1;

    public void setDifficultValue(int difficultValue) {
        this.difficultValue = difficultValue;
        currentSpeed += difficultValue;
    }

    private int difficultValue = 0;

    private int currentTime = 0;
    private SnakeListener snakeListener;

    public SnakeModel(int startX, int startY, SnakeListener snakeListener) {
        super(startX, startY);
        this.snakeListener = snakeListener;

        currentDirection = Direction.values()[new Random().nextInt(Direction.values().length)];
    }

    public void increaseLength() {
        currentLength++;
    }

    public void increaseSpeed() {
        if (currentSpeed < (MAX_SPEED + difficultValue)) {
            currentSpeed++;
        }
    }

    public void decreaseSpeed() {
        if (currentSpeed > (MIN_SPEED + difficultValue)) {
            currentSpeed--;
        }
    }

    public Direction getCurrentDirection() {
        return currentDirection;
    }

    @Override
    public void updateFrame(int time) {
        currentTime += time;

        if (currentTime >= (MAX_SPEED - currentSpeed) * 50) {
            currentTime = 0;

            makeAStep();
        }
    }

    @Override
    public int getLifeTime() {
        return INFINITY_LIFE_TIME;
    }

    public void changeDirection(Direction direction) {
        currentDirection = direction;
    }

    public PointModel getHeadPoint() {
        return points.peekFirst();
    }

    public void makeAStep() {
        PointModel headPoint = getHeadPoint();

        int newX = headPoint.getX();
        int newY = headPoint.getY();

        switch (currentDirection) {
            case UP:
                newY -= 1;
                break;
            case DOWN:
                newY += 1;
                break;
            case LEFT:
                newX -= 1;
                break;
            case RIGHT:
                newX += 1;
                break;
        }

        PointModel newPoint = new PointModel(newX, newY);

        if (hasPoint(newPoint)) {
            snakeListener.died();
            return;
        }

        points.addFirst(newPoint);
        if (points.size() > currentLength) {
            points.removeLast();
        }

        snakeListener.makeAStep();
    }

    private boolean hasPoint(PointModel checkPoint) {
        for (PointModel point : points) {
            if (point.getX() == checkPoint.getX()
                    && point.getY() == checkPoint.getY()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public char getPersonCharacter() {
        return '@';
    }

    /**
     * Created by user on 23.03.2016.
     */
    public enum Direction {
        UP, DOWN, LEFT, RIGHT
    }

    public interface SnakeListener {
        void makeAStep();
        void died();
    }
}