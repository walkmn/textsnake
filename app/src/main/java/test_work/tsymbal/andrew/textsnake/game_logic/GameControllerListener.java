package test_work.tsymbal.andrew.textsnake.game_logic;

public interface GameControllerListener {
    void onGameOver();
}
