package test_work.tsymbal.andrew.textsnake;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import test_work.tsymbal.andrew.textsnake.game_logic.GameController;
import test_work.tsymbal.andrew.textsnake.game_logic.GameControllerListener;

public class MainActivity extends AppCompatActivity implements GameControllerListener {

    public static int FIELD_WIDTH = 20;
    public static int FIELD_HEIGHT = 20;

    private static final int TIME_FOR_UPDATE_FRAME = 1;

    private Timer mTimer = new Timer();

    private TextView tvGameField;
    private TextView tvDifficulty;
    private TextView tvTimeCounter;
    private Button btnStartPause;
    private Button btnSettings;

    private GameController gameController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        setUpFieldSize();

        tvGameField = (TextView) findViewById(R.id.gameField);
        tvDifficulty = (TextView) findViewById(R.id.tvDifficulty);
        tvTimeCounter = (TextView) findViewById(R.id.tvTimeCounter);
        btnStartPause = (Button) findViewById(R.id.btnStartPause);
        btnSettings = (Button) findViewById(R.id.btnSettings);

        gameController = new GameController(this, tvGameField, tvTimeCounter, this);
        gameController.pause();

        btnStartPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnStartPause.getText().toString().contentEquals(getString(R.string.start))) {
                    btnStartPause.setText(getString(R.string.pause));
                    gameController.start();
                } else {
                    btnStartPause.setText(getString(R.string.start));
                    gameController.pause();
                }
            }
        });

        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String[] mDiffName = {getString(R.string.easy), getString(R.string.normal), getString(R.string.hard)};

                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(getString(R.string.choose_difficult_level))
                        .setItems(mDiffName, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int item) {
                                tvDifficulty.setText(mDiffName[item]);
                                gameController.setDifficultLevel(item);
                            }
                        })
                        .setCancelable(false)
                        .create()
                        .show();
            }
        });

        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                gameController.updateFrame(TIME_FOR_UPDATE_FRAME);
            }
        }, 0, TIME_FOR_UPDATE_FRAME);
    }

    @Override
    protected void onResume() {
        super.onResume();

        setUpFieldSize();
    }

    private void setUpFieldSize() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            FIELD_WIDTH = 25;
            FIELD_HEIGHT = 20;
        } else {
            FIELD_WIDTH = 20;
            FIELD_HEIGHT = 25;
        }

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mTimer.cancel();
    }

    @Override
    public void onGameOver() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(getString(R.string.game_is_over_message))
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                gameController.restart();
                                tvDifficulty.setText(getString(R.string.easy));
                                btnStartPause.setText(getString(R.string.start));
                            }
                        })
                        .setCancelable(false)
                        .create()
                        .show();

            }
        });
    }
}