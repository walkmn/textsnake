package test_work.tsymbal.andrew.textsnake;

import android.app.Activity;
import android.widget.TextView;

public class TimerController {

    private Activity activity;
    private TextView timerVidget;

    private int currentMs = 0;
    private int currentS = 0;
    private int currentM = 0;

    public TimerController(Activity activity, TextView timerVidget) {
        this.activity = activity;
        this.timerVidget = timerVidget;
    }

    public void reset() {
        currentMs = 0;
        currentS = 0;
        currentM = 0;

        showTime();
    }

    public void updateFrame(int time) {

        currentMs += time;

        if (currentMs > 1000) {
            currentMs = 0;
            currentS++;
        }
        if (currentS > 60) {
            currentM++;
        }

        showTime();
    }

    private void showTime() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                timerVidget.setText(activity.getString(R.string.timer,
                        String.format("%02d", currentM),
                        String.format("%02d", currentS),
                        String.format("%03d", currentMs)
                ));
            }
        });
    }
}
