package test_work.tsymbal.andrew.textsnake.models.game_models;

public interface AffectingSnake {
    void affect(SnakeModel snake);
}
