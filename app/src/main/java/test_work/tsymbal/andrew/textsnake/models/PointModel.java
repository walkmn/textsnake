package test_work.tsymbal.andrew.textsnake.models;

public class PointModel {

    private int x;
    private int y;

    public PointModel(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
