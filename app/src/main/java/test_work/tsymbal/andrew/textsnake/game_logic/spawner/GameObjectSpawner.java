package test_work.tsymbal.andrew.textsnake.game_logic.spawner;

import java.util.Random;
import test_work.tsymbal.andrew.textsnake.models.game_models.GameModel;

public abstract class GameObjectSpawner {

    private static final int MIN_TIME  = 5000;
    private static final int MAX_TIME  = 15000;

    private int currentTime = 0;

    private int timeForSpawn;

    private int fieldWeight;
    private int fieldHeight;

    public GameObjectSpawner(int fieldWeight, int fieldHeight) {
        this.fieldWeight = fieldWeight;
        this.fieldHeight = fieldHeight;
        chooseYourDestiny();
    }

    public GameModel spawn() {
        int x = new Random().nextInt(fieldWeight);
        int y = new Random().nextInt(fieldHeight);

        return getGameObjForSpawn(x , y);
    }

    public GameModel tryToSpawn(int time) {
        currentTime += time;

        if (currentTime >= timeForSpawn) {
            currentTime = 0;
            chooseYourDestiny();
            return spawn();
        }

        // no in this time
        return null;
    }

    protected abstract GameModel getGameObjForSpawn(int x, int y);

    private void chooseYourDestiny() {
        timeForSpawn = MIN_TIME + (new Random().nextInt(MAX_TIME - MIN_TIME));
    }
}
