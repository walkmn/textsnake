package test_work.tsymbal.andrew.textsnake.models.game_models.bonus_models;

import test_work.tsymbal.andrew.textsnake.models.game_models.AffectingSnake;
import test_work.tsymbal.andrew.textsnake.models.game_models.GameModel;
import test_work.tsymbal.andrew.textsnake.models.game_models.SnakeModel;

public class SpeedUpBonusModel extends GameModel implements AffectingSnake {

    public SpeedUpBonusModel(int startX, int startY) {
        super(startX, startY);
    }

    @Override
    public int getLifeTime() {
        return 16000;
    }

    @Override
    public char getPersonCharacter() {
        return 'S';
    }

    @Override
    public void affect(SnakeModel snake) {
        killYourSelf();

        snake.increaseSpeed();
    }
}
