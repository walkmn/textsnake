package test_work.tsymbal.andrew.textsnake.models.game_models;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Stack;

import test_work.tsymbal.andrew.textsnake.models.PointModel;

public abstract class GameModel {

    private int currentTime = 0;

    protected Deque<PointModel> points = new ArrayDeque<>();
    protected static final int INFINITY_LIFE_TIME = -1;

    public abstract char getPersonCharacter();

    public GameModel(int startX, int startY) {
        points.addFirst(new PointModel(startX, startY));
    }

    public Deque<PointModel> getPoints() {
        return points;
    }
    protected void killYourSelf() {
        points.clear();
    }

    public void updateFrame(int time) {

        if (getLifeTime() == INFINITY_LIFE_TIME) return;

        currentTime += time;

        if (currentTime >= getLifeTime()) {
            currentTime = 0;

            killYourSelf();
        }
    };

    public abstract int getLifeTime();
}
