package test_work.tsymbal.andrew.textsnake.models.game_models;

public class FoodModel extends GameModel implements AffectingSnake {

    public FoodModel(int startX, int startY) {
        super(startX, startY);
    }

    @Override
    public int getLifeTime() {
        return 10000;
    }

    @Override
    public char getPersonCharacter() {
        return '$';
    }

    @Override
    public void affect(SnakeModel snake) {
        killYourSelf();

        snake.increaseLength();
        snake.increaseSpeed();
    }
}
