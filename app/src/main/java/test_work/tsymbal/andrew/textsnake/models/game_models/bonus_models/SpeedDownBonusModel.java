package test_work.tsymbal.andrew.textsnake.models.game_models.bonus_models;

import test_work.tsymbal.andrew.textsnake.models.game_models.AffectingSnake;
import test_work.tsymbal.andrew.textsnake.models.game_models.GameModel;
import test_work.tsymbal.andrew.textsnake.models.game_models.SnakeModel;

public class SpeedDownBonusModel extends GameModel implements AffectingSnake {

    public SpeedDownBonusModel(int startX, int startY) {
        super(startX, startY);
    }

    @Override
    public int getLifeTime() {
        return 6000;
    }

    @Override
    public char getPersonCharacter() {
        return 'D';
    }

    @Override
    public void affect(SnakeModel snake) {
        killYourSelf();

        snake.decreaseSpeed();
    }
}
